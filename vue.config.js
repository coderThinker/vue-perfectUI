module.exports = {
  //关闭代码检测
  lintOnSave: false,
  devServer: {
    port: 8888,
    open: true,
  },
  productionSourceMap:false, //取消map打包
  configureWebpack: {
    resolve: {
      //配置别名
      alias: {
        "assets": "@/assets",
        "common": "@/common",
        "components": "@/components",
        "network": "@/network",
        "views": "@/views"
      }
    }
  },
  pages: {
    index: {
      // page 的入口
      entry: 'src/main.js',
      // 模板来源
      template: 'public/index.html',
      // 在 dist/index.html 的输出
      filename: 'index.html',
    }
  },
  publicPath: '/vue_perfectUI'
}
