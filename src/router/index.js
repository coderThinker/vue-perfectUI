import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

//首页
const home = () => import('views/home/index.vue')
//不支持页
const noSupport = () => import('views/nosupport/index.vue')

//base组件
const button = () => import('views/demo/base/button/index.vue')
const radio = () => import('views/demo/base/radio/index.vue')
const checkbox = () => import('views/demo/base/checkbox/index.vue')
const Switch = () => import('views/demo/base/switch/index.vue')
const badge = () => import('views/demo/base/badge/index.vue')
const tags = () => import('views/demo/base/tags/index.vue')
const count = () => import('views/demo/base/count/index.vue')
const toast = () => import('views/demo/base/toast/index.vue')
const progress = () => import('views/demo/base/progress/index.vue')
const slider = () => import('views/demo/base/slider/index.vue')

//high组件
const noticebar = () => import('views/demo/high/noticebar/index.vue')
const search = () => import('views/demo/high/search/index.vue')
const subsection = () => import('views/demo/high/subsection/index.vue')
const cell = () => import('views/demo/high/cell/index.vue')
const table = () => import('views/demo/high/table/index.vue')
const grid = () => import('views/demo/high/grid/index.vue')
const picker = () => import('views/demo/high/picker/index.vue')
const scroll = () => import('views/demo/high/scroll/index.vue')
const lazyload = () => import('views/demo/high/lazyload/index.vue')
const dropdown = () => import('views/demo/high/dropdown/index.vue')

//super组件
const waterfall = () => import('views/demo/super/waterfall/index.vue')
const popup = () => import('views/demo/super/popup/index.vue')
const tabbar = () => import('views/demo/super/tabbar/index.vue')
const swiper = () => import('views/demo/super/swiper/index.vue')
const tabs = () => import('views/demo/super/tabs/index.vue')
const steps = () => import('views/demo/super/steps/index.vue')
const indexlist = () => import('views/demo/super/indexlist/index.vue')
const barrage = () => import('views/demo/super/barrage/index.vue')

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/noSupport',
      component: noSupport,
      meta: {
        title: 'noSupport'
      }
    },
    {
      path: '/home',
      component: home,
      meta: {
        title: 'vue-perfectUI移动端组件库'
      }
    },
    //base组件
    {
      path: '/button',
      component: button,
      meta: {
        title: 'button 按钮'
      }
    },
    {
      path: '/radio',
      component: radio,
      meta: {
        title: 'radio 单选框'
      }
    },
    {
      path: '/checkbox',
      component: checkbox,
      meta: {
        title: 'checkbox 复选框'
      }
    },
    {
      path: '/switch',
      component: Switch,
      meta: {
        title: 'switch 开关'
      }
    },
    {
      path: '/badge',
      component: badge,
      meta: {
        title: 'badge 徽标数'
      }
    },
    {
      path: '/tags',
      component: tags,
      meta: {
        title: 'tags 标签'
      }
    },
    {
      path: '/count',
      component: count,
      meta: {
        title: 'count 计数器'
      }
    },
    {
      path: '/toast',
      component: toast,
      meta: {
        title: 'toast 弹出提示'
      }
    },
    {
      path: '/progress',
      component: progress,
      meta: {
        title: 'progress 进度条'
      }
    },
    {
      path: '/slider',
      component: slider,
      meta: {
        title: 'slider 滑动选择器'
      }
    },
    //high组件
    {
      path: '/noticebar',
      component: noticebar,
      meta: {
        title: 'noticebar 滚动提示'
      }
    },
    {
      path: '/search',
      component: search,
      meta: {
        title: 'search 搜索框'
      }
    },
    {
      path: '/subsection',
      component: subsection,
      meta: {
        title: 'subsection 分段器'
      }
    },
    {
      path: '/cell',
      component: cell,
      meta: {
        title: 'cell 单元格列表'
      }
    },
    {
      path: '/table',
      component: table,
      meta: {
        title: 'table 表格'
      }
    },
    {
      path: '/grid',
      component: grid,
      meta: {
        title: 'grid 宫格布局'
      }
    },
    {
      path: '/picker',
      component: picker,
      meta: {
        title: 'picker 选择器'
      }
    },
    {
      path: '/scroll',
      component: scroll,
      meta: {
        title: 'scroll 滚动容器'
      }
    },
    {
      path: '/lazyload',
      component: lazyload,
      meta: {
        title: 'lazyload 图片懒加载'
      }
    },
    {
      path: '/dropdown',
      component: dropdown,
      meta: {
        title: 'dropdown 下拉菜单'
      }
    },
    //super组件
    {
      path: '/waterfall',
      component: waterfall,
      meta: {
        title: 'waterfall 瀑布流'
      }
    },
    {
      path: '/popup',
      component: popup,
      meta: {
        title: 'popup 弹窗对话框'
      }
    },
    {
      path: '/tabbar',
      component: tabbar,
      meta: {
        title: 'tabbar 底部导航'
      }
    },
    {
      path: '/swiper',
      component: swiper,
      meta: {
        title: 'swiper 轮播图'
      }
    },
    {
      path: '/tabs',
      component: tabs,
      meta: {
        title: 'tabs 顶部导航'
      }
    },
    {
      path: '/steps',
      component: steps,
      meta: {
        title: 'steps 步骤条'
      }
    },
    {
      path: '/indexlist',
      component: indexlist,
      meta: {
        title: 'indexlist 字母索引列表'
      }
    },
    {
      path: '/barrage',
      component: barrage,
      meta: {
        title: 'barrage 弹幕'
      }
    },
  ]
});


//全局导航守卫
router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  if (to.meta.title != 'vue-perfectUI移动端组件库' && to.meta.title != 'noSupport') {
    document.title = 'Demo'
  }
  //符合条件才跳转
  if (window.innerWidth < 768 || to.meta.title == 'noSupport') {
    next()
  }
})

export default router;
