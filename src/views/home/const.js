//组件数据对象
const list = [
  //基本组件
  {
    head: 'base组件',
    body: [{
        name: 'button 按钮',
        icon: '/vue_perfectUI/img/list/button.svg',
        route: '/button', //此处独有信息
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0, //0为箭头，1为switch开关，2为徽标数，3为自定义图标
          count: 1000, //徽标数量
          src: '' //自定义图标路径
        }
      },
      {
        name: 'radio 单选框',
        icon: '/vue_perfectUI/img/list/radio.svg',
        route: '/radio',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'checkbox 复选框',
        icon: '/vue_perfectUI/img/list/checkbox.svg',
        route: '/checkbox',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'switch 开关',
        icon: '/vue_perfectUI/img/list/switch.svg',
        route: '/switch',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'badge 徽标数',
        icon: '/vue_perfectUI/img/list/badge.svg',
        route: '/badge',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'tags 标签',
        icon: '/vue_perfectUI/img/list/tags.svg',
        route: '/tags',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'count 计数器',
        icon: '/vue_perfectUI/img/list/count.svg',
        route: '/count',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'toast 弹出提示',
        icon: '/vue_perfectUI/img/list/toast.svg',
        route: '/toast',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'progress 进度条',
        icon: '/vue_perfectUI/img/list/progress.svg',
        route: '/progress',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'slider 滑动选择器',
        icon: '/vue_perfectUI/img/list/slider.svg',
        route: '/slider',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
    ]
  },
  //高级组件
  {
    head: 'high组件',
    body: [{
        name: 'noticebar 滚动提示',
        icon: '/vue_perfectUI/img/list/noticebar.svg',
        route: '/noticebar',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'search 搜索框',
        icon: '/vue_perfectUI/img/list/search.svg',
        route: '/search',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'subsection 分段器',
        icon: '/vue_perfectUI/img/list/subsection.svg',
        route: '/subsection',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'cell 单元格列表',
        icon: '/vue_perfectUI/img/list/cell.svg',
        route: '/cell',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'table 表格',
        icon: '/vue_perfectUI/img/list/table.svg',
        route: '/table',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'grid 宫格布局',
        icon: '/vue_perfectUI/img/list/grid.svg',
        route: '/grid',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'picker 选择器',
        icon: '/vue_perfectUI/img/list/picker.svg',
        route: '/picker',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'scroll 滚动容器',
        icon: '/vue_perfectUI/img/list/scroll.svg',
        route: '/scroll',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'lazyload 图片懒加载',
        icon: '/vue_perfectUI/img/list/lazyload.svg',
        route: '/lazyload',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'dropdown 下拉菜单',
        icon: '/vue_perfectUI/img/list/dropdown.svg',
        route: '/dropdown',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
    ]
  },
  //超级组件
  {
    head: 'super组件',
    body: [{
        name: 'waterfall 瀑布流',
        icon: '/vue_perfectUI/img/list/waterfall.svg',
        route: '/waterfall',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'popup 弹窗对话框',
        icon: '/vue_perfectUI/img/list/popup.svg',
        route: '/popup',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'tabbar 底部导航',
        icon: '/vue_perfectUI/img/list/tabbar.svg',
        route: '/tabbar',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'swiper 轮播图',
        icon: '/vue_perfectUI/img/list/swiper.svg',
        route: '/swiper',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'tabs 顶部导航',
        icon: '/vue_perfectUI/img/list/tabs.svg',
        route: '/tabs',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'steps 步骤条',
        icon: '/vue_perfectUI/img/list/steps.svg',
        route: '/steps',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'indexlist 字母索引列表',
        icon: '/vue_perfectUI/img/list/indexlist.svg',
        route: '/indexlist',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
      {
        name: 'barrage 弹幕',
        icon: '/vue_perfectUI/img/list/barrage.svg',
        route: '/barrage',
        describe: '',
        rightText: '',
        rightIcon: {
          type: 0,
          count: 1000,
          src: ''
        }
      },
    ]
  }

]


export default list
